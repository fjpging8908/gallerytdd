from django.contrib.auth.models import User
from django.test import TestCase, Client

# Create your tests here.
from .models import Image
from .models import  Portfolio
from .models import Profile, Product
import json

# Create your tests here.

class GalleryTestCase(TestCase):
    def test_list_portafolios_status(self):
        url = '/gallery/ListPortafolio/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)

    def test_list_portafolios_cantidad(self):
        user_model = User.objects.create_user(username='test', password='123456', first_name='test',
                                              last_name='test', email='test@test.com')
        profile_model =Profile.objects.create(user=user_model, professionalProfile='testprofile',urlPicture='urltest')
        Portfolio.objects.create(user=profile_model)

        response = self.client.get('/gallery/ListPortafolioCantidad/')
        current_data = json.loads(response.content)
        print(current_data)
        self.assertEqual(current_data, 1)

    def test_register_user(self):
        response = self.client.post('/gallery/addUser/', json.dumps(
            {"username": "testUser", "first_name": "Test", "last_name": "User", "password": "123456",
             "email": "test@test.com"}), content_type='application/json')
        current_data = json.loads(response.content)
        self.assertEqual(current_data[0]['fields']['username'], 'testUser')

    def test_view_publicPortfolio(self):
        user_model = User.objects.create_user(username='test', password='123456', first_name='test',
                                              last_name='test', email='test@test.com')
        profile_model = Profile.objects.create(user=user_model, professionalProfile='testprofile', urlPicture='urltest')
        Portafolio = Portfolio.objects.create(user=profile_model)
        Product.objects.create(title="ProductA", urlImage="UrlImageProductA.png", description="This is a description A",
                               type=".png", isPublic=True, idPortfolio=Portafolio)
        Product.objects.create(title="ProductB", urlImage="UrlImageProductB.png", description="This is a description B",
                               type=".png", isPublic=True, idPortfolio=Portafolio)
        Product.objects.create(title="ProductC", urlImage="UrlImageProductB.jpg", description="This is a description C",
                               type=".jpg", isPublic=False, idPortfolio=Portafolio)
        response = self.client.get('/gallery/viewpublicportfolio/' + str(profile_model.id) + '/')
        current_data = json.loads(response.content)
        self.assertEqual(len(current_data), 2)
    def test_login(self):
        Usuario = User.objects.create_user(username='test', password='123456', first_name='test',last_name='test', email='test@test.com')
        response = self.client.post('/gallery/login/', json.dumps({"username": Usuario.username, "password": "123456"}), content_type='application/json')
        current_data = json.loads(response.content)
        self.assertEqual(current_data['Result'], True)

    def test_editUser(self):
        Usuario = User.objects.create_user(username='TestUser', password='123455', first_name='FirstName',last_name='LastName', email='User@User.com')
        response = self.client.put('/gallery/editUser/' + str(Usuario.id) + '/', json.dumps({"first_name": "FirstNameChange", "last_name": "LastNameChange", "email": "NewUserEmail@Email.com"}),content_type='application/json')
        current_data = json.loads(response.content)
        self.assertEqual(current_data['first_name'], "FirstNameChange")
