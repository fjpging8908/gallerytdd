from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Image
from .models import Portfolio
from .models import Product
from django.contrib.auth import authenticate

import json

# Create your views here.
@csrf_exempt
def index(request):
    images_list = Image.objects.all()
    return HttpResponse(serializers.serialize("json", images_list))

def ListPortafolio(request):
     portafolio_list =Portfolio.objects.all()
     return  HttpResponse(serializers.serialize("json",portafolio_list))


def ListPortafolioCantidad(request):
    portafolio_list = Portfolio.objects.all()
    return HttpResponse(len(portafolio_list))

@csrf_exempt
def add_register_view(request):
    if request.method == 'POST':
        json_user = json.loads(request.body)
        username = json_user['username']
        first_name = json_user['first_name']
        last_name = json_user['last_name']
        password = json_user['password']
        email = json_user['email']

        user_model = User.objects.create_user(username=username, password=password)
        user_model.first_name = first_name
        user_model.last_name = last_name
        user_model.email = email
        user_model.save()
    return HttpResponse(serializers.serialize("json", [user_model]))

@csrf_exempt
def viewpublicportfolio(request, id):
    Portafolio = Portfolio.objects.filter(user=id)
    products_list = Product.objects.filter(idPortfolio=Portafolio[0].id, isPublic=True)
    return HttpResponse(serializers.serialize("json", products_list))


@csrf_exempt
def login(request):
    if request.method == 'POST':
        jsonUser = json.loads(request.body)
        user = jsonUser['username']
        passw = jsonUser['password']
        user = authenticate(username=user, password=passw)
        if user is not None:
            Result = True
        else:
            Result= False
    return HttpResponse(json.dumps({"Result": Result}))

@csrf_exempt
def edit_personinfo(request, id):
    if request.method == 'PUT':
        json_user = json.loads(request.body)
        user = User.objects.filter(id=id).first()
        user.first_name = json_user['first_name']
        user.last_name = json_user['last_name']
        user.email = json_user['email']
        user.save()

        userjson = {"username": user.username, "first_name": user.first_name, "last_name": user.last_name, "email": user.email}

    return HttpResponse(json.dumps(userjson))