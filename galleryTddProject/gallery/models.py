from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Image(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=1000)
    description = models.CharField(max_length=1000, null=True)
    type = models.CharField(max_length=5, blank=True)
    user = models.ForeignKey(User, null=True, on_delete=models.PROTECT)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    professionalProfile = models.CharField(max_length=200)
    urlPicture = models.CharField(max_length=1000)


class Portfolio(models.Model):
    user = models.ForeignKey(Profile, null=True, on_delete=models.PROTECT)


class Product(models.Model):
    title = models.CharField(max_length=200)
    urlImage = models.CharField(max_length=1000)
    description = models.CharField(max_length=1000, null=True)
    type = models.CharField(max_length=5, blank=True)
    isPublic = models.BooleanField()
    idPortfolio = models.ForeignKey(Portfolio, null=True, on_delete=models.CASCADE)