from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('ListPortafolio/', views.ListPortafolio, name='ListPortafolio'),
    path('ListPortafolioCantidad/', views.ListPortafolioCantidad, name='ListPortafolioCantidad'),
    path('addUser/', views.add_register_view, name='addUser'),
    path('viewpublicportfolio/<int:id>/', views.viewpublicportfolio, name='viewpublicportfolio'),
    path('login/', views.login, name='login'),
    path('editUser/<int:id>/', views.edit_personinfo, name='editUser'),
]
